import importlib
import os
import csv
import sqlite3
import subprocess
import sys
import pandas as pd

# Objeto global de importacao
ghost = importlib.import_module("bin.common_functions") # Módulo fantasma de invocação de funções
seek_ghost = importlib.import_module("bin.seek")

def metaAlign(dados, target, input_flags, topNsize=10, speciesThr=0.7):
    #### LEITURA E ATUALIZAÇÃO DAS AMOSTRAS
    with open(os.path.join(dados["output_directory"], dados["project_name"]+".taxReport.dio")) as outFile:
        tsv_reader = csv.reader(outFile, delimiter="\t")
        # Recuperando o nome das amostras baseado no cabeçalho do resultado da Taxonomia
        for line in tsv_reader:
            samples = line[3:]
            break

    ##### ENCONTRAR O TOP N DAS AMOSTRAS
    #### Mudando a pasta de trabalho
    # Se o usuário não desejar somente alinhar, faça a construção do genoma de interesse
    # (Se fizessemos somente pela flag da presença de "--only-build", em caso de ausência de
    # ambas as flags esse trecho de código não seria executado)
    if "--only-align" not in input_flags:
        os.chdir(dados["output_directory"])
        subprocess.call("rm -f "+dados["project_name"]+".log.dio", shell=True)  # Removendo o arquivo de logs antigo para criar um novo
        greatestBacteria = get_topN(dados=dados, target=target, topNsize=topNsize, input_flags=input_flags, speciesThr=speciesThr)

        #### ANALISANDO O BANCO DE DADOS, BAIXANDO OS GENOMAS E CRIANDO O GENOMA DE INTERESSE
        # Criando a pasta de resultados
        subprocess.call("rm -rf results; mkdir -p results", shell=True) # Remover o diretório de resultados, caso ele exista, e criar um novo
        dados["output_directory"] = os.path.join(dados["output_directory"],"results") # Mudando o diretório de trabalho
        os.chdir(dados["output_directory"]) # Entrando no diretório de trabalho

        subprocess.call("mkdir -p tmpGenomes", shell=True)
        os.chdir(dados["output_directory"]+"/tmpGenomes")
        createGenomeofInterest(greatestBacteria=greatestBacteria, dados=dados, ghost=ghost, input_flags=input_flags, target=target) # Função diferente entre Selfish e Groupal

        # Se o usuário desejar somente a opção de construir o genoma de interesse, saia e finalize.
        if "--only-build" in input_flags:
            # Salvar em um json todas as infos dessa análise para o usuário saber se o savepoint que ele está usando é o correto

            # PROCESSANDO AS INFORMAÇÕES (Sensibilidade taxonômica)
            taxSens = "Species"
            if "--byGenus" in input_flags:
                taxSens = "Genus"

            seek_ghost.savepoint(mode="w", input_args=[target, "Selfish", taxSens, topNsize, speciesThr])
            sys.exit(0)
    # Se a flag de somente alinhar estiver nos argumentos
    else:
        dados["output_directory"] += "/results"  # Modificando a variável tal qual seria ela no código completo
        os.chdir(dados["output_directory"])

    #### CRIANDO O ÍNDICE DO GENOMA DO SALMON
    # Criando o genoma por amostra
    for i, sample in enumerate(samples):
        ghost.coloredPrint(str(i+1)+"- "+sample, color="White")
        process = subprocess.Popen(["salmon","index","-p",dados["threads"],"-t",sample+".fa","-i",sample,"-k","13","--keepDuplicates"]
                                  ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ghost.monitoring(process, input_flags)

    #### RECUPERANDO AMOSTRAS PARA ALINHAR COM O SALMON
    aux = ghost.getFiles(dados["samples_directory"], pattern=(".fastq", ".fastq.gz"))  # Recuperar as amostras baseado no padrão do sufixo
    # Recuperar o nome das amostras
    # SE: amostra
    # PE: ["1P": [amostra].1P.fastq.gz, "1U": [amostra].1U.fastq.gz,...]
    samples = ghost.formatSamples(aux, dados)

    #### ALINHANDO AS AMOSTRAS
    subprocess.call("mkdir -p tmpQuant", shell=True)
    os.chdir(dados["output_directory"]+"/tmpQuant")
    if "--quiet" not in input_flags:
        ghost.coloredPrint("\nMetaAligning samples:", color="Green")
    seek_ghost.salmonAlign(dados=dados, samples=samples, input_flags=input_flags, specialAlign="Selfish", selfish=True)

    # Escrevendo a versão final dos arquivos de quantificação
    os.chdir(dados["output_directory"])
    aux = ghost.getFiles(dados["output_directory"], ".quant.dio")  # Recuperar as amostras baseado no padrão
    seek_ghost.rewriteQuantFiles(quant_files=aux, byGenus=("--byGenus" in input_flags and target == "Bact")) # POSSÍVEL PONTO DE ACELERAÇÃO - Todos os arquivos do Salmon tem a mesma ordem

    # Alinhando amostras não pareadas
    if dados["sample_format"] == "PE" and dados["sample_usage"] == "P&U":
        subprocess.call("mkdir -p unpaired", shell=True)
        os.chdir(dados["output_directory"]+"/tmpQuant")
        if "--quiet" not in input_flags:
            ghost.coloredPrint("\nMetaAligning unpaired samples:", color="Green")
        seek_ghost.salmonAlign(dados=dados, samples=samples, input_flags=input_flags, specialAlign="Selfish", mode="U", selfish=True)

        # Escrevendo a versão final dos arquivos de quantificação
        os.chdir(dados["output_directory"]+"/unpaired")
        aux = ghost.getFiles(os.path.join(dados["output_directory"],"unpaired"), ".quant.dio")  # Recuperar as amostras baseado no padrão
        seek_ghost.rewriteQuantFiles(quant_files=aux, mode="U", byGenus=("--byGenus" in input_flags and target == "Bact"))

    # Finalizando a etapa do Salmon e excluindo as pastas temporárias
    os.chdir(dados["output_directory"])
    command = ["rm", "-r", "tmpQuant"]
    for sample in samples:
        command.append(sample)
    subprocess.call(" ".join(command), shell=True) # Deletando a pasta dos genomas

def get_topN(dados, target, topNsize, input_flags, speciesThr):
    ##### ENCONTRAR O TOP N DAS AMOSTRAS
    taxReport = pd.read_csv(dados["project_name"]+".taxReport.dio", sep="\t") # Ler o arquivo e filtrar linhas e colunas

    # Análise a nível de Gênero
    if target == "Bact" and "--byGenus" in input_flags:
        # Recuperando os dados de gênero da tabela original
        genus_df = taxReport[taxReport["Rank"] == "genus"].copy()
        cond = genus_df["Species"].str.contains("Homo") | genus_df["Species"].str.contains("virus")
        genus_df = genus_df.drop(genus_df[cond].index.values) # Eliminar linhas baseadas nos filtros

        # Eliminando a coluna Rank e preparando o dataframe para as etapas posteriores
        del genus_df["Rank"]
        reads_df = genus_df
    # Análise a nível de espécie
    else:
        # Filtrar baseado no foco da análise
        aux = taxReport[taxReport["Rank"] == "species"].copy() # Filtrar somente por dados de espécie
        if target == "Bact":
            cond = aux["Species"].str.contains("virus") | aux["Species"].str.contains("synthetic") | aux["Species"].str.contains("Homo") # Filtros
            aux = aux.drop(aux[cond].index.values) # Eliminar linhas baseadas nos filtros
        if target == "Vir":
            cond = aux["Species"].str.contains("virus")
            aux = aux[cond]

        del aux["Rank"]
        reads_df = aux

    # Normalizando por RPM os dados
    pd.options.mode.chained_assignment = None # Desativar avisos de cópia
    reads_df = seek_ghost.calculateRPM(reads_df=reads_df)

    # Cria o top N por amostra, seja gênero ou espécie
    samples = reads_df.columns.values.tolist()[2:] # Lista de amostras
    tid_by_sample, sp_by_sample = {}, {} # Lista de espécies por amostra

    # Obtendo o top N de espécies ou gêneros por amostra e seus respectivos taxids
    for sample in samples:
        species = reads_df.sort_values(by=sample, ascending=False)

        # Criando as matrizes de taxid e espécies
        taxid, species = species["Taxid"].head(topNsize).values.tolist(), species["Species"].head(topNsize).values.tolist()
        tid_by_sample[sample], sp_by_sample[sample] = taxid, species

    if target == "Bact" and "--byGenus" in input_flags:
        nogenusAssociated = False
        lostGenusbySample = {}

        # Filtrar somente por dados de espécie e descobrir a espécie que melhor representa o gênero
        aux = taxReport.copy()
        aux = aux[aux["Rank"] == "species"]
        aux.insert(2,"Genus",taxReport["Species"].str.split(" ", expand=True)[0]) # Inserir uma coluna gênero para facilitar as coisas
        aux = seek_ghost.calculateRPM(reads_df=aux, offset=4)

        # {"APS": {"Escherichia": {"Escherichia coli": 562, "Escherichia hermanii": 10342}, "Salmonella": {...}}, ...}
        greatestBacteria = {}
        for i,sample in enumerate(samples):
            greatestBacteria[sample] = {}
            genus = sp_by_sample[sample] # Lista de top N gêneros na amostra

            for gen in genus:
                greatestBacteria[sample][gen] = None # Se preparando para uma possível não-associação

                # Avaliando o gênero baseado no dataframe contendo as espécies
                subset = aux[aux["Genus"] == gen].copy() # Copiando somente o gênero desejado
                subset = subset.sort_values(by=sample, ascending=False) # Ordenando baseado na maior espécie daquele gênero na amostra analisada

                # Métrica de ponderamento
                columns = subset.columns[4:]
                ponder = subset[columns].mean(axis=1) * (subset[columns] > 0).sum(axis=1)/len(columns)  # Ponder = Média * Presença/N
                ponder = (ponder - ponder.min()) / (ponder.max() - ponder.min())  # Mins and Maxs normalization

                # Retornando somente as linhas que alcançam a condição da métrica de ponderamento
                ponder = ponder >= speciesThr
                score, lines = ponder.axes[0].tolist(), ponder.values.tolist()  # Convertendo o dataframe de score para
                score = [score[i] for i, elem in enumerate(lines) if elem]  # Filtrando para retornar somente as linhas que alcançaram o threshold desejado

                # Inserindo a bactéria escolhida
                # Garantindo o formato: {"APS": {"Escherichia": {"Escherichia coli": 562, "Escherichia hermanii": 10342}, "Salmonella": {...}}, ...}
                aux1, aux2 = subset[subset.index.isin(score)]["Species"].values.tolist(), subset[subset.index.isin(score)]["Taxid"].values.tolist()
                # Inserindo a top espécie na lista
                greatestBacteria[sample][gen] = {sp:aux2[i] for i,sp in enumerate(aux1)}

                # Checar se o dicionário de gêneros está vazio, insira como espécie órfã e processe o alinhamento
                if not greatestBacteria[sample][gen]:
                    greatestBacteria[sample][gen] = None

                    if sample not in lostGenusbySample:
                        lostGenusbySample[sample] = []

                    nogenusAssociated = True
                    lostGenusbySample[sample].append(gen)

        # SALVAR O ARQUIVO DE GÊNEROS ÓRFÃOS CASO SEJA PRECISO
        if nogenusAssociated:
            if "--quiet" not in input_flags:
                ghost.coloredPrint("Some genera could not be associated to a species. These genus and its respective samples will be saved at "+dados["project_name"] + ".log.dio.", color="Yellow")

           # Salvar o arquivo final
            orphanBlack = [] # Lista para gerar o arquivo de saída para os gêneros órfãos
            for sample in lostGenusbySample:
                aux = lostGenusbySample[sample]
                aux.insert(0, sample+":")

                orphanBlack.append(aux)
            orphanBlack.insert(0, ["Orphan genus by sample:"])
            orphanBlack.append([])

            with open(dados["project_name"] + ".log.dio", "w") as outFile:
                tsv_writer = csv.writer(outFile, delimiter="\t")
                tsv_writer.writerows(orphanBlack)
    # Caso não satisfaça a condição de bactérias e por gênero
    # O formato {"APS": {"Escherichia": {"Escherichia coli": 562, "Escherichia hermanii": 10342}, "Salmonella": {...}}, ...} deve ser garantido aqui também
    else:
        greatestBacteria = {}

        for i,sample in enumerate(samples):
            greatestBacteria[sample] = {sp:{sp:tid_by_sample[sample][i]} for i,sp in enumerate(sp_by_sample[sample])}

    return greatestBacteria

def processAndSave(input, output, id_string):
    output.append([id_string])
    output.append(input.columns.values.tolist())
    output.extend(input.values.tolist())
    output.append([])

    return (output)

def createGenomeofInterest(greatestBacteria, dados, ghost, input_flags, target):
    #### CONECTANDO À BASE DE DADOS
    conn = sqlite3.connect(dados["installation_path"]+"/lib/genomes.db")
    c = conn.cursor()

    #### ANALISANDO O BANCO DE DADOS E BAIXANDO OS GENOMAS
    if "--quiet" not in input_flags:
        ghost.coloredPrint("Downloading and builiding Top N genomes:", color="Green")

    # Checar se a tabela RefSeq existe para então usar ela
    c.execute("""SELECT COUNT(*) FROM sqlite_master WHERE type="table" AND name="RefSeq";""")
    refSeqExists = bool(c.fetchall()[0][0])

    #genomes = {} # Dicionário para guardar os genomas por espécie

    # Criando a tradução de taxid para nome de espécie: Interagindo por linha dos dataframes (Ambos tem o mesmo tamanho)
    # ENTRADA: {"APS": {"Escherichia": {"Escherichia coli": 562, "Escherichia hermanii": 10342}, "Salmonella": {...}}, ...}
    # Obtendo as espécies de cada download e determinando quais devem ser baixadas para evitar downloads múltiplos
    # SAÍDA: {"Escherichia coli": 562, "Salmonella enterica": blabla, ...}
    sp2tid = {}
    for sample in greatestBacteria:
        for genus in greatestBacteria[sample]:
            # Se o gênero for vazio, continue. Não imprima na tela.
            if greatestBacteria[sample][genus] is None:
                continue

            # Por que não itera pelas chaves, mas só funciona assim?????
            for sp in greatestBacteria[sample][genus].keys():
                if sp not in sp2tid:
                    sp2tid[sp] = greatestBacteria[sample][genus][sp] # Inserindo a espécie e o taxid no dicionário de download

    orphanBlack = [] # Genomas não associados
    sp2link = {} # Dicionários de espécies associadas e nome do genoma

    for count, species in enumerate(sp2tid):
        taxid = sp2tid[species]

        # Print inicial de nomenclatura
        if "--quiet" not in input_flags:
            if "--byGenus" in input_flags and target == "Bact":
                genus = species.split(" ")[0]
                ghost.coloredPrint(str(count+1)+"/"+str(len(sp2tid))+" - "+species+" as "+genus, color="White")
            else:
                ghost.coloredPrint(str(count+1)+"/"+str(len(sp2tid))+" - "+species, color="White")

        ### ESSA FUNÇÃO É IGUAL NOS DOIS MODOS.
        # POSSÍVEL MELHORIA: Criar um método conjunto em caso de manutenção.
        # Procurar o genoma da espécie na base de dados
        genome_file = seek_ghost.searching_db(db=c, taxid=taxid, mode="Std")
        # Caso não ache no modo padrão, tente o RefSeq
        if genome_file is None:
            # Procurar no RefSeq, caso exista
            if refSeqExists:
                genome_file = seek_ghost.searching_db(db=c, taxid=taxid, mode="RefSeq")

            # Se não achar o genoma, pergunte se deseja pesquisar outro Taxid
            if genome_file is None and "--dontDisturb" not in input_flags:
                if "--quiet" not in input_flags:
                    ghost.coloredPrint("Genome could not be found. You could try updating your database with the latest RefSeq release available through the 'updateDB' mode.", color="Red")

                genome_file = seek_ghost.searchSpecies(db=c, species=species, refSeqExists=refSeqExists)

        # Se não achar o genoma, pule a espécie
        # Associando um link à uma espécie, mesmo que o link não exista
        sp2link[species] = genome_file

        if genome_file is None:
            # Caso seja uma espécie sem genoma encontrado, adicione na lista o nome dela.
            if species not in orphanBlack:
                orphanBlack.append([species])

            if "--quiet" not in input_flags:
                ghost.coloredPrint("Genome could not be found.", color="Red")
            continue

        # Extraindo os genomas (.gz)
        subprocess.call("gunzip "+genome_file+".fna.gz", shell=True)
        subprocess.call("gunzip "+genome_file+".gff.gz", shell=True)

        # Criar o .gtf
        process = subprocess.Popen(["gffread","-T",genome_file+".gff", "-o",genome_file+".gtf"]
                                    ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ghost.monitoring(process, input_flags)
        seek_ghost.create_translateTable(genome_file, species, taxid, byGenus=("--byGenus" in input_flags and target == "Bact")) # Construir a tabela de tradução (gene id, symbol, species, taxid) a partir do GFF

        # Criar o .fa
        process = subprocess.Popen(["gffread","-w",genome_file+".fa","-g",genome_file+".fna","-T",genome_file+".gtf"]
                                    ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ghost.monitoring(process, input_flags)

    # SALVANDO GENOMAS QUE NÃO PUDERAM SER ACHADOS
    # Caso haja algum genoma não associado
    if len(orphanBlack) > 0:
        # Finalizando a formatação
        orphanBlack.insert(0, ["Genomes that could not be downloaded:"])
        orphanBlack.append([])

        if "--quiet" not in input_flags:
            ghost.coloredPrint("Some genomes could not be found. These genomes will be saved at "+dados["project_name"] + ".log.dio.",
            color="Red")

        # Salvando o arquivo
        with open(os.path.join(dados["original_output_directory"], dados["project_name"] + ".log.dio"), "a") as outFile:
            tsv_writer = csv.writer(outFile, delimiter="\t")
            tsv_writer.writerows(orphanBlack)

    # Criando o genoma de interesse e movendo para a pasta de trabalho
    if "--quiet" not in input_flags:
        print()
        ghost.coloredPrint("Merging top N genomes and creating the genome of interest...\n", color="Green")

    ### GENOMA CONJUNTO
    # Criando o genoma por amostra
    for sample in greatestBacteria:
        species2bind = [] # Lista de top N espécies para cada amostra

        # Calculando espécies presentes no grupo analisado
        if "--byGenus" in input_flags:
            genus = greatestBacteria[sample].keys()  # Retornando os gêneros presentes nas amostras e se preparando para retornar as espécies
            for gen in genus:
                # Se o gênero for vazio, continue. Não imprima na tela.
                if greatestBacteria[sample][gen] is None:
                    continue

                species = greatestBacteria[sample][gen].keys()  # Espécies associadas por gênero
                species2bind.extend(species)  # Pode haver repetição
        else:
            species2bind = greatestBacteria[sample].keys()

        command = [sp2link[sp]+".fa" for sp in species2bind if sp2link[sp] is not None] # Associando as espécies aos genomas baixados anteriormente - Evitar gêneros sem espécie associada (--byGenus) e genomas não baixados
        command.insert(0, "cat")
        command.extend([">", sample+".fa"])
        subprocess.call(" ".join(command), shell=True) # Efetuando o comando

    # # Movendo o genoma e apagando as pastas temporárias
    subprocess.call("mv .translate.txt "+dados["output_directory"], shell=True)
    command = [sample+".fa" for sample in greatestBacteria]
    command.insert(0, "mv")
    command.append(dados["output_directory"])
    subprocess.call(" ".join(command), shell=True)
    os.chdir(dados["output_directory"])
    subprocess.call("rm -r tmpGenomes", shell=True)
