import sqlite3, csv, os, importlib, subprocess
import pandas as pd
import sys
import json

# Objeto global de importacao
ghost = importlib.import_module("bin.common_functions") # Módulo fantasma de invocação de funções
seek_ghost = importlib.import_module("bin.seek")

def metaAlign(dados, target, input_flags, topNsize=10, speciesThr=0.7, sampleMetadata=None):
    #### LEITURA E ATUALIZAÇÃO DAS AMOSTRAS
    with open(os.path.join(dados["output_directory"], dados["project_name"]+".taxReport.dio")) as outFile:
        tsv_reader = csv.reader(outFile, delimiter="\t")
        # Recuperando o nome das amostras baseado no cabeçalho do resultado da Taxonomia
        for line in tsv_reader:
            samples = line[3:]
            break

    ##### ENCONTRAR O TOP N DAS AMOSTRAS
    # Se o arquivo de metadado das amostras não for nulo descubra o tipo das amostras
    specialAlign = None # Avaliar a necessidade de um alinhamento específico (Tipo de amostra)
    if sampleMetadata != None:
        (sampleMetadata,specialAlign) = getType(sampleMetadata, samples)

        # Se após a checagem a lista de amostras estiver vazia, encerre a execução
        if not sampleMetadata:
            ghost.coloredPrint("Your samples' list is empty! CHECK YOUR SAMPLES! This execution will be TERMINATED!", color="Red")
            sys.exit(1)

    #### Mudando a pasta de trabalho
    os.chdir(dados["output_directory"])
    subprocess.call("rm -f " + dados["project_name"] + ".log.dio", shell=True)  # Removendo o arquivo de logs de genomas órfãos na associação

    # Se o usuário não desejar somente alinhar, faça a construção do genoma de interesse
    # (Se fizessemos somente pela flag da presença de "--only-build", em caso de ausência de
    # ambas as flags esse trecho de código não seria executado)
    if "--only-align" not in input_flags:
        if specialAlign != None:
            ghost.coloredPrint("Using samples "+str(sampleMetadata)+" as reference for all other samples", color="Green")
            greatestBacteria = get_topN(dados=dados, target=target, topNsize=topNsize, input_flags=input_flags, speciesThr=speciesThr, metadata=sampleMetadata)
        else:
            greatestBacteria = get_topN(dados=dados, target=target, topNsize=topNsize, input_flags=input_flags, speciesThr=speciesThr)

        #### ANALISANDO O BANCO DE DADOS, BAIXANDO OS GENOMAS E CRIANDO O GENOMA DE INTERESSE
        # Criando a pasta de resultados
        subprocess.call("rm -rf results; mkdir -p results", shell=True) # Remover o diretório de resultados, caso ele exista, e criar um novo
        dados["output_directory"] = os.path.join(dados["output_directory"],"results") # Mudando o diretório de trabalho
        os.chdir(dados["output_directory"]) # Entrando no diretório de trabalho

        subprocess.call("mkdir -p tmpGenomes", shell=True)
        os.chdir(dados["output_directory"]+"/tmpGenomes")
        createGenomeofInterest(greatestBacteria=greatestBacteria, dados=dados, ghost=ghost, input_flags=input_flags, target=target) # Função diferente entre Selfish e Groupal

        # Se o usuário desejar somente a opção de construir o genoma de interesse, saia e finalize.
        if "--only-build" in input_flags:
            # Salvar em um json todas as infos dessa análise para o usuário saber se
            # o savepoint que ele está usando é o correto

            # PROCESSANDO AS INFORMAÇÕES
            # Tipo de alinhamento
            alignmentType = "Groupal"
            if sampleMetadata is not None:
                alignmentType = "Metadata"

            # Sensibilidade taxonômica
            taxSens = "Species"
            if "--byGenus" in input_flags:
                taxSens = "Genus"

            seek_ghost.savepoint(mode="w", input_args=[target, alignmentType, taxSens, topNsize, speciesThr, specialAlign])
            sys.exit(0)
    # Se a flag de somente alinhar estiver nos argumentos
    else:
        dados["output_directory"] += "/results" # Modificando a variável tal qual seria ela no código completo
        os.chdir(dados["output_directory"])

    #### CRIANDO O ÍNDICE DO GENOMA DO SALMON
    process = subprocess.Popen(["salmon","index","-p",dados["threads"],"-t","genome.fa","-i","genome","-k","13","--keepDuplicates"]
                              ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    ghost.monitoring(process, input_flags)

    #### RECUPERANDO AMOSTRAS PARA ALINHAR COM O SALMON
    # Escolhendo o sufixo da amostra, baseado no tipo
    aux = ghost.getFiles(dados["samples_directory"], pattern=(".fastq", ".fastq.gz"))  # Recuperar as amostras baseado no padrão do sufixo
    # Recuperar o nome das amostras
    # SE: amostra
    # PE: ["1P": [amostra].1P.fastq.gz, "1U": [amostra].1U.fastq.gz,...]
    samples = ghost.formatSamples(aux, dados)

    #### ALINHANDO AS AMOSTRAS
    subprocess.call("mkdir -p tmpQuant", shell=True)
    os.chdir(dados["output_directory"]+"/tmpQuant")
    if "--quiet" not in input_flags:
        ghost.coloredPrint("MetaAligning samples:", color="Green")
    seek_ghost.salmonAlign(dados=dados, samples=samples, input_flags=input_flags)

    # Escrevendo a versão final dos arquivos de quantificação
    os.chdir(dados["output_directory"])
    aux = ghost.getFiles(dados["output_directory"], ".quant.dio")  # Recuperar as amostras baseado no padrão
    seek_ghost.rewriteQuantFiles(quant_files=aux, byGenus=("--byGenus" in input_flags)) # POSSÍVEL PONTO DE ACELERAÇÃO - Todos os arquivos do Salmon tem a mesma ordem

    # Alinhando amostras não pareadas
    if dados["sample_format"] == "PE" and dados["sample_usage"] == "P&U":
        subprocess.call("mkdir -p unpaired", shell=True)
        os.chdir(dados["output_directory"]+"/tmpQuant")
        if "--quiet" not in input_flags:
            ghost.coloredPrint("MetaAligning unpaired samples:", color="Green")
        seek_ghost.salmonAlign(dados=dados, samples=samples, input_flags=input_flags, mode="U")

        # Escrevendo a versão final dos arquivos de quantificação
        os.chdir(dados["output_directory"]+"/unpaired")
        aux = ghost.getFiles(os.path.join(dados["output_directory"],"unpaired"), ".quant.dio")  # Recuperar as amostras baseado no padrão
        seek_ghost.rewriteQuantFiles(quant_files=aux, mode="U", byGenus=("--byGenus" in input_flags))

    # Finalizando a etapa do Salmon e excluindo as pastas temporárias
    os.chdir(dados["output_directory"])
    subprocess.call("rm -r tmpQuant genome", shell=True)

def getType(sampleMetadata, samples):
    # Se o arquivo de metadado das amostras não for nulo (Checar se todos os arquivos tem amostras)
    ans = None
    metadata = {}

    if sampleMetadata != None:
        sampleMetadata = os.path.abspath(sampleMetadata)

        # Ler o arquivo e encontrar os tipos de amostras
        with open(sampleMetadata, "r") as file:
            tsv_reader = csv.reader(file, delimiter="\t")

            for row in tsv_reader:
                type, sample = row[1], row[0]

                if type not in metadata:
                    metadata[type] = []

                metadata[type].append(sample)

        # Escolher quais tipos de amostra usar
        print("------------ Found "+str(len(metadata))+" sample types. Choose what you'd like to use:")
        for i, type in enumerate(metadata):
            print("["+str(i)+"] -", type)
        while(True):
            ans = int(input("------------ From [0,"+str(len(metadata)-1)+"] enter the sample type you want to use as reference for all samples: "))

            if ans in range(len(metadata)):
                ans = list(metadata.keys())[ans]
                break
            else:
                ghost.coloredPrint("Try again:", color="Red")

    metadata = metadata[ans] # Retorna somente as amostras do tipo desejado

    # Checar se existem amostras no metadado que não tem correspondência nas amostras analisadas
    not_intersect = [elem for elem in metadata if elem not in samples]
    if not_intersect:
        ghost.coloredPrint("MESSAGE: "+str(not_intersect)+" is/are not in analyzed samples and therefore will not be used. If you think this is a mistake check your metadata file!", color="Yellow")
        metadata = [elem for elem in metadata if elem in samples]

    return (metadata,ans)

def filterSamples(reads_df, metadata):
    # Filtrar as amostras por tipo, caso desejado
    colnames = ["Taxid", "Species"]
    colnames.extend(metadata)
    reads_df = reads_df[colnames]

    return reads_df

def get_topN(dados, target, topNsize, input_flags, speciesThr, metadata=None):
    ##### ENCONTRAR O TOP N DAS AMOSTRAS
    taxReport = pd.read_csv(dados["project_name"]+".taxReport.dio", sep="\t") # Ler o arquivo e filtrar linhas e colunas

    # Análise a nível de Gênero
    if target == "Bact" and "--byGenus" in input_flags:
        # Recuperando os dados de gênero da tabela original
        genus_df = taxReport[taxReport["Rank"] == "genus"].copy()
        cond = genus_df["Species"].str.contains("Homo") | genus_df["Species"].str.contains("virus") # Filtros
        genus_df = genus_df.drop(genus_df[cond].index.values) # Eliminar linhas baseadas nos filtros

        # Eliminando a coluna Rank e preparando o dataframe para as etapas posteriores
        del genus_df["Rank"]
        reads_df = genus_df
    # Análise a nível de espécie
    else:
        # Filtrar baseado no foco da análise
        aux = taxReport[taxReport["Rank"] == "species"].copy() # Filtrar somente por dados de espécie
        if target == "Bact":
            cond = aux["Species"].str.contains("virus") | aux["Species"].str.contains("synthetic") | aux["Species"].str.contains("Homo") # Filtros
            aux = aux.drop(aux[cond].index.values) # Eliminar linhas baseadas nos filtros
        if target == "Vir":
            cond = aux["Species"].str.contains("virus")
            aux = aux[cond]

        del aux["Rank"]
        reads_df = aux

    # Filtrar as amostras por tipo, caso desejado
    if metadata != None:
        reads_df = filterSamples(reads_df=reads_df, metadata=metadata)

    # Normalizando por RPM os dados e ordenando
    pd.options.mode.chained_assignment = None # Desativar avisos de cópia
    reads_df = seek_ghost.calculateRPM(reads_df=reads_df)
    reads_df = reads_df.head(topNsize) # Manter somente o Top N desejado

    # VARIÁVEL PARA RETORNAR OS MICRORGANISMOS A SEREM BAIXADOS
    # {"Escherichia": {"Escherichia coli": 562, "Escherichia hermanii": 10342}, "Salmonella": {...}}
    greatestBacteria = {sp:None for sp in reads_df["Species"].values.tolist()}

    # Rotina para descobrir quais espécies melhor representam o gênero
    if target == "Bact" and "--byGenus" in input_flags:
        orphanGenus = []
        noGenusAssociated = False # Flag para avisar sobre gêneros não associados

        aux = taxReport.copy()
        aux = aux[aux["Rank"] == "species"]
        aux.insert(2,"Genus",taxReport["Species"].str.split(" ", expand=True)[0])
        aux = seek_ghost.calculateRPM(reads_df=aux, offset=4)

        # Descobrindo qual espécie melhor representa o gênero
        for i, genus in enumerate(greatestBacteria):
            # Avaliando o gênero baseado no dataframe contendo as espécies
            subset = aux[aux["Genus"] == genus].copy()

            # Trabalhar com os metadados, caso exista
            if metadata != None:
                subset = filterSamples(reads_df=subset, metadata=metadata)

            # Caso não encontre nenhuma espécie associada, atribua "None" e continue o laço
            if subset.empty:
                noGenusAssociated = True
                orphanGenus.append(genus)
                continue

            # Assegurar que a bactéria escolhida esteja presente na maioria, senão todas as amostras, e evitar enviesamento pela média
            columns = subset.columns[4:]
            ponder = subset[columns].mean(axis=1) * (subset[columns] > 0).sum(axis=1)/len(columns) # Ponder = Média * Presença/N
            ponder = (ponder - ponder.min())/(ponder.max() - ponder.min()) # Mins and Maxs normalization

            # Retornando somente as linhas que alcançam a condição da métrica de ponderamento
            ponder = ponder >= speciesThr
            score, lines = ponder.axes[0].tolist(), ponder.values.tolist() # Convertendo o dataframe de score para
            score = [score[i] for i,elem in enumerate(lines) if elem] # Filtrando para retornar somente as linhas que alcançaram o threshold desejado

            # Inserindo a bactéria escolhida
            # Garantindo o formato: {"Escherichia": {"Escherichia coli": 562, "Escherichia hermanii": 10342}, "Salmonella": {...}}
            aux1, aux2 = subset[subset.index.isin(score)]["Species"].values.tolist(), subset[subset.index.isin(score)]["Taxid"].values.tolist()
            greatestBacteria[genus] = {sp:aux2[i] for i,sp in enumerate(aux1)}

        # SALVAR O ARQUIVO DE GÊNEROS ÓRFÃOS CASO SEJA PRECISO
        if noGenusAssociated:
            if "--quiet" not in input_flags:
                ghost.coloredPrint("Some genera could not be associated to a species. These genus names will be saved at " + dados["project_name"] + ".log.dio.", color="Red")

            # Salvar o arquivo final
            orphanBlack = [["Genus that could not be associated to a species:"]]  # Lista para gerar o arquivo de saída para os gêneros órfãos
            for genus in orphanGenus:
                orphanBlack.append([genus])
            orphanBlack.append([])

            with open(dados["project_name"] + ".log.dio", "w") as outFile:
                tsv_writer = csv.writer(outFile, delimiter="\t")
                tsv_writer.writerows(orphanBlack)
    else:
        # Garantindo o formato: {"Escherichia coli": {"Escherichia coli": 562}, "Salmonella enterica": {...}}
        aux1, aux2 = reads_df["Species"].values.tolist(), reads_df["Taxid"].values.tolist()
        for i, taxon in enumerate(greatestBacteria):
            greatestBacteria[taxon] = {aux1[i]:aux2[i]}

    return greatestBacteria

def createGenomeofInterest(greatestBacteria, dados, ghost, input_flags, target):
    #### CONECTANDO À BASE DE DADOS
    conn = sqlite3.connect(dados["installation_path"]+"/lib/genomes.db")
    c = conn.cursor()

    #### ANALISANDO O BANCO DE DADOS E BAIXANDO OS GENOMAS
    if "--quiet" not in input_flags:
        ghost.coloredPrint("Downloading and builiding Top N genomes:", color="Green")

    # Checar se a tabela RefSeq existe para então usar ela
    c.execute("""SELECT COUNT(*) FROM sqlite_master WHERE type="table" AND name="RefSeq";""")
    refSeqExists = bool(c.fetchall()[0][0])

    count = 0 # Contagem
    orphanSpecies = {} # Lista de genomas órfãos

    for index, taxon in enumerate(greatestBacteria):
        count += 1

        # Filtro para determinar caso um gênero não tenha sido associada a um espécie
        if "--byGenus" in input_flags and target == "Bact" and greatestBacteria[taxon] is None:
            if "--quiet" not in input_flags:
                genus = taxon
                ghost.coloredPrint(str(count)+"/"+str(len(greatestBacteria))+ + " - "+ genus, color="White")
                ghost.coloredPrint("This genus could not be associated to a species.", color="Red")
                continue

        # Impressão inicial na tela de taxons (1 - Escherichia) ou (1 - Helicobacter pylori)
        if "--quiet" not in input_flags:
            ghost.coloredPrint(str(count)+"/"+str(len(greatestBacteria))+" - "+taxon, color="White")

        # Impressão das espécies associadas por gênero, caso existam
        #   1.1 - Escherichia coli
        #   1.2 - Escherichia hermanii
        for subIndex, species in enumerate(greatestBacteria[taxon]):
            # Impressão na tela somente caso espécies associadas por gênero existam,
            # caso contrário o tamanho do dicionário será sempre igual a 1 e não haverá
            # necessidade de uma impressão
            if "--byGenus" in input_flags and target == "Bact" and "--quiet" not in input_flags:
                subIndex += 1
                ghost.coloredPrint("\t"+str(subIndex)+"/"+str(len(greatestBacteria[taxon]))+" - "+species, color="White")

            taxid = int(greatestBacteria[taxon][species]) # Converter taxid para inteiro e facilitar a operação

            ### ESSA FUNÇÃO É IGUAL NOS DOIS MODOS.
            # POSSÍVEL MELHORIA: Criar um método conjunto em caso de manutenção.
            # Procurar o genoma da espécie na base de dados
            genome_file = seek_ghost.searching_db(db=c, taxid=taxid, mode="Std")
            # Caso não ache no modo padrão, tente o RefSeq
            if genome_file is None:
                # Procurar no RefSeq, caso exista
                if refSeqExists:
                    genome_file = seek_ghost.searching_db(db=c, taxid=taxid, mode="RefSeq")

                # Se não achar o genoma, pergunte se deseja pesquisar outro Taxid
                if genome_file is None and "--dontDisturb" not in input_flags:
                    if "--quiet" not in input_flags:
                        ghost.coloredPrint("Genome could not be found. You could try updating your database with the latest RefSeq release available through the 'updateDB' mode.", color="Red")

                    genome_file = seek_ghost.searchSpecies(db=c, species=species, refSeqExists=refSeqExists)

                # Se não achar o genoma, pule a espécie
                if genome_file is None:
                    orphanSpecies[species] = taxid

                    if "--quiet" not in input_flags:
                        ghost.coloredPrint("Genome could not be found.", color="Red")
                    continue

            # Extraindo os genomas (.gz)
            subprocess.call("gunzip "+genome_file+".fna.gz", shell=True)
            subprocess.call("gunzip "+genome_file+".gff.gz", shell=True)

            # Criar o .gtf
            process = subprocess.Popen(["gffread","-T",genome_file+".gff", "-o",genome_file+".gtf"]
                                        ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            ghost.monitoring(process, input_flags)
            seek_ghost.create_translateTable(genome_file, species, taxid, byGenus=("--byGenus" in input_flags and target == "Bact")) # Construir a tabela de tradução (gene id, symbol, species, taxid) a partir do GFF

            # Criar o .fa
            process = subprocess.Popen(["gffread","-w",genome_file+".fa","-g",genome_file+".fna","-T",genome_file+".gtf"]
                                        ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            ghost.monitoring(process, input_flags)

    # SALVANDO GENOMAS QUE NÃO PUDERAM SER ACHADOS
    # Genomas não associados
    orphanBlack = [[sp, orphanSpecies[sp]] for sp in orphanSpecies] # (SP - TAXID)

    # Caso haja algum genoma não associado
    if len(orphanBlack) > 0:
        # Finalizando a formatação
        orphanBlack.insert(0, ["Genomes that could not be downloaded (Sp - Taxid):"])
        orphanBlack.append([])

        if "--quiet" not in input_flags:
            ghost.coloredPrint("\nSome genomes could not be found. These genomes will be saved at " + dados["project_name"] + ".log.dio.\n", color="Red")

        # Salvando o arquivo
        with open(os.path.join(dados["original_output_directory"], dados["project_name"] + ".log.dio"), "a") as outFile:
            tsv_writer = csv.writer(outFile, delimiter="\t")
            tsv_writer.writerows(orphanBlack)

    # Criando o genoma de interesse e movendo para a pasta de trabalho
    if "--quiet" not in input_flags:
        ghost.coloredPrint("Merging top N genomes and creating the genome of interest...\n", color="Green")

    # Criando o genoma conjunto
    subprocess.call("cat *.fa > genome.fa", shell=True)

    # Movendo o genoma e apagando as pastas temporárias
    subprocess.call("mv genome.fa .translate.txt "+dados["output_directory"], shell=True)
    os.chdir(dados["output_directory"])
    subprocess.call("rm -r tmpGenomes", shell=True)
