import importlib
import sqlite3, subprocess, os, csv
from sys import stdout

def update_db(install_path):
    os.chdir(install_path)
    subprocess.call("mkdir -p tmpDB", shell=True)
    os.chdir(install_path+"/tmpDB")

    # Objeto global de importacao
    ghost = importlib.import_module("bin.common_functions") # Módulo fantasma de invocação de funções

    # Fazer o download
    ghost.coloredPrint("MESSAGE: Downloading database...", color="Green")
    subprocess.call("wget -c https://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt", shell=True)

    ghost.coloredPrint("MESSAGE: Updating database... Don't interrupt it.", color="Green")
    # Descobrir o número de linhas do arquivo
    with open("assembly_summary_refseq.txt", "r") as file:
        size = len(file.readlines())

    # Se conectar à base de dados
    conn = sqlite3.connect(install_path+"/lib/genomes.db")
    c = conn.cursor()

    # Deletando a tabela anterior, caso exista
    c.execute("DROP TABLE IF EXISTS RefSeq;")

    # Criando as tables do Genbank e Refseq
    create_refseq_string = """CREATE TABLE IF NOT EXISTS RefSeq (
    id integer AUTO INCREMENT PRIMARY KEY, 
    assembly_accession text NOT NULL,
    bioproject text,
    biosample text,
    refseq_category text,
    taxid text NOT NULL,
    species_taxid text NOT NULL,
    organism_name text NOT NULL,
    infraspecific_name text,
    ftp_path text NOT NULL);"""
    c.execute(create_refseq_string)

    # Genbank tem uma header diferente do RefSeq
    header_id = [0,1,2,4,5,6,7,8,19]
    insert_list = []

    with open("assembly_summary_refseq.txt", "r") as genome_file:
        read_tsv = csv.reader(genome_file, delimiter="\t")

        for i,row in enumerate(read_tsv):
            # Pular as duas primeiras linhas
            if i < 2:
                continue

            input_data = [row[i].replace("strain=", "").replace("ftp://", "https://") if row[i] != "" else None for i in header_id]
            insert_list.append(tuple(input_data))

            if i % 10000 == 0 or i == (size-1):
                stdout.write("\rLoading RefSeq file - %f%%" %((i+1)/size * 100))
    print()

    # Deletar a table se ela existir
    c.executemany("""INSERT INTO RefSeq (assembly_accession,bioproject,biosample,refseq_category,taxid,species_taxid,organism_name,infraspecific_name,ftp_path)
                VALUES (?,?,?,?,?,?,?,?,?);""", insert_list)
    print("Inserted %d references!" %size)
    conn.commit()
    conn.close()

    os.chdir(install_path)
    subprocess.call("rm -r tmpDB", shell=True)
    ghost.coloredPrint("MESSAGE: Configuration ended.", color="Green")
