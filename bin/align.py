import importlib, subprocess, os, json
import pandas as pd
import sys

# Objeto global de importacao
ghost = importlib.import_module("bin.common_functions") # Módulo fantasma de invocação de funções

def align(config_file, db_centrifuge, db_recentrifuge, input_flags, confScore=25):
    #### LER O ARQUIVO DE CONFIGURAÇÃO
    with open(config_file, 'r') as json_file:
        dados = json.load(json_file)

    if dados["sample_format"] == "PE" and dados["sample_usage"] is None:
        ghost.coloredPrint("Check again your samples. There is some kind of mistake. PAIRED SAMPLES COULD NOT BE FOUND.", color="Red")
        sys.exit(1)

    # Converter possíveis caminhos relativos para absoluto
    db_centrifuge = os.path.abspath(db_centrifuge)
    db_recentrifuge = os.path.abspath(db_recentrifuge)

    aux = ghost.getFiles(dados["samples_directory"], pattern=(".fastq", ".fastq.gz"))  # Recuperar as amostras baseado no padrão do sufixo
    # Recuperar o nome das amostras
    # SE: amostra
    # PE: ["1P": [amostra].1P.fastq.gz, "1U": [amostra].1U.fastq.gz,...]
    samples = ghost.formatSamples(aux, dados)

    #### GERANDO A PASTA DO PROCESSO E MUDANDO O DIRETÓRIO DE TRABALHO PARA O GERADO PELO PROGRAMA
    subprocess.call("mkdir "+dados["output_directory"], shell=True)
    os.chdir(dados["output_directory"])

    #### FORMATANDO O COMANDO-BASE
    base_com_centrifuge = ["centrifuge", "-p", dados["threads"], "-x", db_centrifuge, "--report-file", "-S"]
    base_com_recentrifuge = ["rcf", "-f", "-n", db_recentrifuge, "-e", "TSV", "-y", str(confScore)]

    output_dir = dados["output_directory"]
    input_dir = dados["samples_directory"]

    # Caso o processamento seja Paired e Unpaired crie uma pasta temporária --------- FALTA ATUALIZAR PARA .FASTQ.GZ
    if dados["sample_usage"] == "P&U":
        subprocess.call("mkdir -p unpaired", shell=True)

        # As amostras já estão recuperadas em samples e aux
        # Juntando as amostras .1U e .2U
        if "--quiet" not in input_flags:    ghost.coloredPrint("Merging unpaired samples...", color="Green")
        for id, i in enumerate(samples):
            ghost.coloredPrint(i+" - "+str(id+1)+"/"+str(len(samples)), color="White")

            # Checar se as amostras estão descompactadas, caso contrário descompactar
            if ".gz" in samples[i]["1U"]:
                subprocess.call("gzip -d "+samples[i]["1U"])
                samples[i]["1U"].replace(".gz", "")
            if ".gz" in samples[i]["2U"]:
                subprocess.call("gzip -d " + samples[i]["2U"])
                samples[i]["2U"].replace(".gz", "")

            # Juntar as amostras não pareadas em uma só. Comprimir em uma amostra só.
            subprocess.call("cat "+os.path.join(input_dir,samples[i]["1P"])+" "+os.path.join(input_dir,samples[i]["2P"])+" > unpaired/"+i+".U.fastq", shell=True)
            subprocess.call("gzip "+i+".U.fastq", shell=True)
        if "--quiet" not in input_flags:    print()

    ##### CENTRIFUGE E RECENTRIFUGE
    for id, i in enumerate(samples):
        sample = i

        ###### FORMATANDO O COMANDO POR AMOSTRA PARA O CENTRIFUGE
        command_list = base_com_centrifuge[:] # Comando final por amostra
        aux = []

        if dados["sample_format"] == "SE":
            aux = [os.path.join(input_dir,samples[sample])]
        elif dados["sample_format"] == "PE":
            # Inserir os dados das amostras PE
            aux = ["-1", os.path.join(input_dir,samples[sample]["1P"]),
                   "-2", os.path.join(input_dir,samples[sample]["2P"])]

            if dados["sample_usage"] == "P&U":
                aux.extend(["-U", "unpaired/"+sample+".U.fastq.gz"])

        for elem in aux: command_list.insert(-2, elem) # Inserir os dados no comando final
        command_list.insert(-1, sample+".rep.txt") # Arquivo final do report
        command_list.append(sample+".res.txt") # Arquivo final de saída do Centrifuge

        ###### FORMATANDO O COMANDO POR AMOSTRA PARA O RECENTRIFUGE
        recentrifuge_command = base_com_recentrifuge[:]
        recentrifuge_command.insert(2, os.path.join(output_dir,sample+".res.txt"))

        # Executando o comando
        if "--quiet" not in input_flags:
            ghost.coloredPrint(sample+" - "+str(id+1)+"/"+str(len(samples)), color="White")

        # Centrifuge
        if "--quiet" not in input_flags:
            ghost.coloredPrint("Executing Centrifuge...", color="Green")
        process = subprocess.Popen(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ghost.monitoring(process, input_flags)

        # ReCentrifuge
        if "--quiet" not in input_flags:
            ghost.coloredPrint("Executing Recentrifuge...", color="Green")
        process = subprocess.Popen(recentrifuge_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ghost.monitoring(process, input_flags)
        if "--quiet" not in input_flags:
            print()

    generateTaxReport(dados=dados, samples=samples)
    generateTaxStat(dados=dados, samples=samples)
    subprocess.call("rm *.res.txt.rcf.html *.rep.txt *.res.txt *.res.txt.rcf.stat.tsv *.res.txt.rcf.data.tsv", shell=True)

def generateTaxReport(dados, samples):
    for i,sample in enumerate(samples):
        aux = pd.read_csv(sample+".res.txt.rcf.data.tsv", sep="\t", skiprows=[0,1,2], usecols=[0,1,4,5])
        aux.columns = ["Taxid", sample, "Rank", "Species"] # Nomeando as colunas
        aux = aux[["Taxid", "Rank", "Species", sample]]

        if i == 0:
            reads_df = aux
        else:
            # Outer merge dos dataframes
            reads_df = reads_df.merge(aux, how="outer", on=["Taxid", "Rank", "Species"])

        # Preencher NaN com 0
        reads_df = reads_df.fillna(0)

    # Salvar o arquivo da taxonomia
    reads_df.to_csv(dados["project_name"]+".taxReport.dio", sep="\t", index=False)

def generateTaxStat(dados, samples):
    for i,sample in enumerate(samples):
        aux = pd.read_csv(sample+".res.txt.rcf.stat.tsv", sep="\t", skiprows=0)
        aux.columns = ["Data", sample]
        #aux = aux.set_index("Data")

        if i == 0:
            stats_df = aux
        else:
            # Outer merge dos dataframes
            stats_df = stats_df.merge(aux, how="outer", on=["Data"])

    # Salvar o arquivo
    stats_df.to_csv(dados["project_name"]+".taxStat.dio", sep="\t", index=False)

