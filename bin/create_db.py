import os
import csv
import sqlite3

os.chdir("../lib")
conn = sqlite3.connect("genomes.db")
c = conn.cursor()

create_string = """CREATE TABLE IF NOT EXISTS Genomes (
id integer AUTO INCREMENT PRIMARY KEY, 
assembly_accession text NOT NULL,
bioproject text NOT NULL,
biosample text,
refseq_category text NOT NULL,
taxid text NOT NULL,
species_taxid text NOT NULL,
organism_name text NOT NULL,
infraspecific_name text,
ftp_path text NOT NULL);"""

c.execute(create_string)

header = ['assembly_accession', 'bioproject', 'biosample', 'wgs_master', 'refseq_category', 'taxid', 'species_taxid', 'organism_name', 
'infraspecific_name', 'isolate', 'version_status', 'assembly_level', 'release_type', 'genome_rep', 'seq_rel_date', 'asm_name', 
'submitter', 'gbrs_paired_asm', 'paired_asm_comp', 'ftp_path', 'excluded_from_refseq', 'relation_to_type_material']
header_id = [0,1,2,4,5,6,7,8,-3]

with open("base.txt") as f:
	read_tsv = csv.reader(f, delimiter="\t")
	
	for i,row in enumerate(read_tsv):
		input_data = [row[i].replace("strain=", "") if row[i] != "" else None for i in header_id]
		print(i)

		insert_string = """INSERT INTO Genomes (assembly_accession,bioproject,biosample,refseq_category,taxid,species_taxid,organism_name,infraspecific_name,ftp_path)
		VALUES (?,?,?,?,?,?,?,?,?)"""
		c.execute(insert_string, tuple(input_data))
		conn.commit()


conn.close()