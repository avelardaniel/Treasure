import csv
import os

def getFiles(path, pattern):
	# Leitura das amostras
	samples = [file for file in os.listdir(path) if file.endswith(pattern)] # Listar os arquivos que terminam com "fastq" no diretório que o programa foi executado
	samples.sort() # Ordenar as amostras

	return(samples)
def formatSamples(input, dados):
	samples = {}

	########## CRIAR O PROCESSAMENTO CASO FALTE ALGUMA AMOSTRA
	if dados["sample_format"] == "SE":
		for file in input:
			sample_name = file.split(".")[0]  # Quebrar o nome do arquivo por ponto. Obter somente a primeira parte: Nome da amostra.
			# Se a amostra ainda não estiver na lista
			if sample_name not in samples:
				samples[sample_name] = file

	elif dados["sample_format"] == "PE":
		for file in input:
			sample_name = file.split(".")[0]  # Quebrar o nome do arquivo por ponto. Obter somente a primeira parte: Nome da amostra.

			# Se a amostra ainda não estiver na lista
			if sample_name not in samples:
				samples[sample_name] = {"1P": None, "2P": None, "1U": None, "2U": None}

			# Caso ela esteja na lista e seja fita forward pareada
			if "1P.fastq" in file:
				samples[sample_name]["1P"] = file
			elif "2P.fastq" in file:
				samples[sample_name]["2P"] = file
			elif "1U.fastq" in file:
				samples[sample_name]["1U"] = file
			elif "2U.fastq" in file:
				samples[sample_name]["2U"] = file

	return samples
def monitoring(process, flags):
	while True:
		val = None # Valor de retorno
		output = process.stderr.readline()

		# Imprimir mediante as flags e também caso a string contenha algo
		if "--increaseVerbose" in flags and "--quiet" not in flags and output.strip():
			print(output.strip())

		# Condição de saída do loop
		return_code = process.poll()
		if return_code is not None:
			output = process.stderr.readline()
			break

	return val
def coloredPrint(input, color=None):
	if color == "Green":
		input = "\033[1;32m"+input+"\033[0m"
	elif color == "White":
		input = "\033[1m"+input+"\033[0m"
	elif color == "Red":
		input = "\033[1;91m"+input+"\033[0m"
	elif color == "Yellow":
		input = "\033[1;93m"+input+"\033[0m"

	print(input)
