import importlib, json, os, multiprocessing
import sys


def config(install_path, out_file, in_dir, out_dir):
	# Objeto global de importacao
	ghost = importlib.import_module("bin.common_functions") # Módulo fantasma de invocação de funções

	# Converter possíveis caminhos relativos para absoluto
	out_file = os.path.abspath(out_file)
	in_dir = os.path.abspath(in_dir)
	out_dir = os.path.abspath(out_dir)

	# Perguntas iniciais
	project_name = input("------------ What will be your project's name?\n")
	while(True):
		threads = int(input("------------ How many threads would you like to use?\n"))
		nThr = multiprocessing.cpu_count()

		if threads > nThr or threads <= 0:
			ghost.coloredPrint("Max number of threads allowed in this computer is "+str(nThr)+". Try again:", color="Red")
		elif threads <= nThr:
			threads = str(threads)
			break
	while(True):
		sample_format = input("------------ Samples are Single-End (SE) or Paired-End (PE)? Use 'SE' or 'PE' to answer.\n")
		sample_format = sample_format.upper()

		if sample_format == "SE" or sample_format == "PE":
			break
		else:
			ghost.coloredPrint("Try again! Use 'SE' or 'PE'.", color="Red")

	aux = ghost.getFiles(in_dir, (".fastq", ".fastq.gz")) # Aceitar arquivos .fastq e .fastq.gz
	sample_usage = None # Variável de controle para amostras PE

	# Avaliando amostras Single-End
	if sample_format == "SE":
		if not aux: # Checando se a lista de amostras está vazia
			ghost.coloredPrint("ERROR: Check again your samples. There is some kind of mistake. SAMPLES COULD NOT BE FOUND. Try again!", color="Red")
			sys.exit(1)
		else: # Mostrar mensagem de aviso caso as amostras sejam encontradas
			ghost.coloredPrint("MESSAGE: Samples were found.", color="Green")
	# Avaliando amostras Paired-End
	elif sample_format ==  "PE":
		termination = {".1P.fastq":[], ".2P.fastq": [], ".1U.fastq":[], ".2U.fastq": []}

		########## CRIAR O PROCESSAMENTO CASO FALTE ALGUMA AMOSTRA
		for file in aux:
			sample_name = file.split(".")[0] # Quebrar o nome do arquivo por ponto. Obter somente a primeira parte: Nome da amostra.

			if ".1P.fastq" in file:
				termination[".1P.fastq"].append(sample_name)
			elif ".2P.fastq" in file:
				termination[".2P.fastq"].append(sample_name)
			elif ".1U.fastq" in file:
				termination[".1U.fastq"].append(sample_name)
			elif ".2U.fastq" in file:
				termination[".2U.fastq"].append(sample_name)

		termination_values = list(termination.values())

		# CHECAR SE AS AMOSTRAS FORAM ENCONTRADAS (Estão com o sufixo correto)
		isEmpty = all([False if elem else True for elem in termination_values])
		if isEmpty:
			ghost.coloredPrint("ERROR: Check again your samples. There is some kind of mistake. SAMPLES COULD NOT BE FOUND. Try again!",color="Red")
			sys.exit(1)

		if termination_values.count(termination_values[0]) == len(termination_values): # Checar se a quantidade de amostras 1P se repete para as restantes (2P,1U,2U)
			while(True):
				ans = input("------------ We found Paired and Unpaired samples. Do you wish to use paired and unpaired samples or only the paired ones? (PU/P)\n")
				ans = ans.upper()

				if ans == "P":
					ghost.coloredPrint("MESSAGE: Using ONLY PAIRED samples.", color="Green")
					sample_usage = "P" # Using paired samples
					break
				elif ans == "PU":
					ghost.coloredPrint("MESSAGE: Using paired and unpaired samples.", color="Green")
					sample_usage = "P&U" # Using paired and unpaired samples
					break
				else:
					ghost.coloredPrint("Try again:", color="Red")
		elif termination_values[0] == termination_values[1]: # Checar se as amostras 1P e 2P são iguais
			ghost.coloredPrint("MESSAGE: Using ONLY PAIRED samples. If you think this is a mistake, check your samples.", color="Green")
			sample_usage = "P" # Using paired samples
		elif sample_usage is None:
			ghost.coloredPrint("ERROR: Check again your samples. There is some kind of mistake. NOT ALL PAIRED SAMPLES COULD BE FOUND. Try again!", color="Red")
			sys.exit(1)

	# Finalizando o arquivo de saída
	out = {"installation_path": install_path,
		   "samples_directory": in_dir,
		   "output_directory": os.path.join(out_dir, project_name),
		   "project_name": project_name,
		   "sample_format": sample_format,
		   "sample_usage": sample_usage,
		   "threads": threads}

	# Salvar no json desejado
	with open(out_file+".config.dio", 'w') as json_file:
		json.dump(out, json_file, indent=4)

	ghost.coloredPrint("MESSAGE: Configuration ended.", color="Green")
