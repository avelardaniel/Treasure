import importlib, subprocess, os, json, csv, copy, sys

# Objeto global de importacao
ghost = importlib.import_module("bin.common_functions") # Módulo fantasma de invocação de funções

def seek2destroy(**kwargs):
    #### LER O ARQUIVO DE CONFIGURAÇÃO
    with open(kwargs["config_file"], 'r') as json_file:
        dados = json.load(json_file)
        dados["original_output_directory"] = dados["output_directory"] # Criando um backup da variável do diretório de trabalho
        kwargs["dados"] = dados
        del kwargs["config_file"] # Deletando para não ter erro na função metaAlign

    # Modo de identificação de savepoint
    if "--only-align" in kwargs["input_flags"]:
        # Alterando o kwargs se necessário (Caso tenhamos que sobreescrever os argumentos)
        kwargs = savepoint(mode="r", kwargs=kwargs)

    try:
        # ESCOLHER O MODO DE ALINHAMENTO GRUPAL OU EGOÍSTA
        if "--selfish" in kwargs["input_flags"]:
            self_ghost = importlib.import_module("bin.selfish")
            self_ghost.metaAlign(**kwargs) # Alinhamento individual, extraindo top N para cada amostra
        else:
            group_ghost = importlib.import_module("bin.groupal")
            group_ghost.metaAlign(**kwargs) # Alinhamento grupal (Todas as amostras ou baseado em metadados)
    # Em caso de interrupção pelo teclado e a flag de somente alinhar não esteja presente, apague a pasta results
    except KeyboardInterrupt:
        if "--only-align" not in kwargs["input_flags"]:
            os.chdir(dados["original_output_directory"])
            subprocess.call("rm -rf results", shell=True)

# Método para criar ou ler um savepoint, caso o usuário deseje somente construir o genoma ou somente alinhar
def savepoint(mode, input_args=None, kwargs=None):
    # input_args é uma lista com informações sobre o alinhamento e utilizada somente no modo de escrita
    # kwargs é a lista de entrada (Usada somente no modo de leitura) dos argumentos que serão utilizados no programa. Essa função pode sobrescrever os métodos enviados pelo usuário, ou seja, alterar os argumentos de entrada.
    # [Tipo de alvo (Bact ou Vir), Tipo de alinhamento (Groupal, Metadata ou Selfish),
    #     Sensibilidade taxonômica (Gênero ou espécie) e Threshold de espécie]
    if mode == "w":
        out = {"target": input_args[0],
               "alignmentType": input_args[1],
               "taxonomicSensibility": input_args[2],
               "topNsize": input_args[3],
               "spThreshold": input_args[4]}

        if len(input_args) > 6:
           out["sampleType"] = input_args[5] # Tipo de amostra utilizado. (Somente em caso de alinhamento do tipo metadata)

        # Salvar no json desejado
        with open(".continue.dio", 'w') as json_file:
            json.dump(out, json_file, indent=4)
    # No modo de leitura, processe os dados do arquivo de continue e pergunte pro usuário,
    # se ele deseja prosseguir com essas configurações
    elif mode == "r":
        path, flags = kwargs["dados"]["output_directory"], kwargs["input_flags"]

        # Lendo o arquivo de savepoint
        with open(path+"/results/.continue.dio", 'r') as json_file:
            parameters = json.load(json_file)

        # Processando o arquivo de saída
        if "--quiet" not in flags:
            ghost.coloredPrint("You're using the only-align mode.", color="Red")
            ghost.coloredPrint("A savepoint file was found and it was generated with these parameters:", color="White")
            if parameters["target"] == "Bact":
                print("Target: Bacteria")
            elif parameters["target"] == "Virus":
                print("Target: Virus")
            print("Alignment type: "+parameters["alignmentType"])
            if parameters["alignmentType"] == "Metadata":
                print("Sample type: "+parameters["sampleType"])
            print("Taxonomic sensibility: "+parameters["taxonomicSensibility"])
            print("Top N size: "+str(parameters["topNsize"]))
            # Processando impressão de tela
            if parameters["taxonomicSensibility"] == "Genus":
                print("Species Threshold: "+str(parameters["spThreshold"]))

        # Avaliando se há necessidade de alterar os argumentos de kwargs
        # AVALIANDO A SENSIBILIDADE TAXONÔMICA
        if "--byGenus" in flags: kwargs["input_flags"].remove("--byGenus")
        if parameters["taxonomicSensibility"] == "Genus":
            kwargs["input_flags"].append("--byGenus")

        # AVALIANDO O TIPO DE ALINHAMENTO
        # Removendo a flag selfish, caso exista. Essa condição trata o modo metadata e groupal como um só, afinal o que os diferencia é somente o cálculo do Top N.
        if "--selfish" in flags: kwargs["input_flags"].remove("--selfish")
        if parameters["alignmentType"] == "Selfish":
            kwargs["input_flags"].append("--selfish")

        # Avisando que os argumentos serão sobrescritos pelos que existem no savepoint. Nem todos argumentos importam.
        if "--quiet" not in flags:
            ghost.coloredPrint("\nAll parameters that you passed will be overwritten by those above.", color="Yellow")

        # Essa porção só deve executar caso nenhuma das duas flags esteja presente: NOT OR (Essa é a porta lógica que descreve isso)
        if (("--dontDisturb" not in flags) and ("--quiet" not in flags)):
            while(True):
                ans = input("\n------------ Do you wish to continue with these parameters? (Y/N)\n")
                ans = ans.upper()

                if ans == "Y":
                    ghost.coloredPrint("\nCreating salmon's index...", color="Green")
                    break
                if ans == "N":
                    ghost.coloredPrint("TERMINATING PROGRAM.", color="Red")
                    sys.exit(1)
                    break
                else:
                    ghost.coloredPrint("Try again:", color="Red")

        return kwargs

# Método de alinhamento do Salmon
def salmonAlign(dados, samples, input_flags, mode=None, selfish=False):
    # Alterando o diretório de trabalho mediante o modo especial ou não
    if mode is None:
        work_dir = dados["output_directory"]
    elif mode == "U":
        work_dir = os.path.join(dados["output_directory"], "unpaired") # Pasta de trabalho e adicionar unpaired

    for i,sample in enumerate(samples):
        # Se o método selfish for o executado, altere a pasta do genoma para ao invés de usar um genoma comum usar um genoma por amostra
        if selfish:
            genomeFolder = sample
        else:
            genomeFolder = "genome"

        base_com_salmonQuant = ["salmon","quant","-p",dados["threads"],"-l","A","-i","../"+genomeFolder,"--validateMappings","-o"]

        ###### FORMATANDO O COMANDO POR AMOSTRA PARA O CENTRIFUGE
        command_list = base_com_salmonQuant[:] # Comando final por amostra
        aux = []

        if "--quiet" not in input_flags:
            ghost.coloredPrint(sample+" - "+str(i+1)+"/"+str(len(samples)), color="White")

        if dados["sample_format"] == "SE":
            name = samples[sample]
            aux = ["-r", os.path.join(dados["samples_directory"],name)]
        elif dados["sample_format"] == "PE" and mode is None:
            fP, rP = samples[sample]["1P"], samples[sample]["2P"]
            aux = ["-1", os.path.join(dados["samples_directory"],fP),
                   "-2", os.path.join(dados["samples_directory"],rP)]
        elif dados["sample_format"] == "PE" and mode == "U":
            aux = ["-r", os.path.join(dados["original_output_directory"],"unpaired",sample+".U.fastq.gz")]

        for elem in aux[::-1]: command_list.insert(8, elem) # Inserindo os dados da amostra no comando final
        command_list.append(sample+"_quant") # Inserindo a pasta para salvar os arquivos de saída do Salmon

        # Quantificando as amostras
        process = subprocess.Popen(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ghost.monitoring(process, input_flags)

        # Movendo o resultado da quantificação para o diretório de trabalho e renomeando o arquivo
        subprocess.call("cp "+sample+"_quant/quant.sf "+os.path.join(work_dir,sample+".quant.dio"), shell=True)
        subprocess.call("cp " + sample + "_quant/logs/salmon_quant.log " + os.path.join(work_dir, sample + ".log.dio"), shell=True)

    if "--quiet" not in input_flags:
        print()

# Reescreve os arquivos de quantificação para conter todas as informações adquiridas pelo programa
def rewriteQuantFiles(quant_files, mode="P", byGenus=False):
    if mode == "P":
         location = ".translate.txt"
    elif mode == "U":
         location = "../.translate.txt"

    translate_table = {}

    with open(location, "r") as file:
        tsv_reader = csv.reader(file, delimiter="\t")

        for row in tsv_reader:
            translate_table[row[0]] = row

    for file in quant_files:
        if byGenus: # Se o resultado for por Gênero, incluir mais uma coluna
            data_file = [["GeneName", "GeneSymbol", "Genus", "Aligned_Species", "Taxid", "Length", "EffectiveLength", "TPM", "NumReads"]]
        else:
            data_file = [["GeneName", "GeneSymbol", "Species", "Taxid", "Length", "EffectiveLength", "TPM", "NumReads"]]

        # Lendo o arquivo original
        with open(file, "r") as sample_file:
            tsv_reader = csv.reader(sample_file, delimiter="\t")
            for i,row in enumerate(tsv_reader):
                if i > 0:
                    original_name = row[0]
                    aux = copy.deepcopy(translate_table[original_name])
                    aux.extend(row[1:])
                    data_file.append(aux)

        # Salvando a nova versão
        with open(file, "w") as sample_file:
            tsv_writer = csv.writer(sample_file, delimiter="\t")
            tsv_writer.writerows(data_file)

# Calcular o RPM de cada amostra
def calculateRPM(reads_df, offset=2):
    # Ordenar o dataframe adequadamente
    reads_df.index = [i for i in range(len(reads_df.index))]

    # Normalizando por RPM os dados e ordenando
    columns = reads_df.columns[offset:]
    reads_df.loc[:,columns] = reads_df[columns].div(reads_df[columns].sum(axis=0), axis=1) * 1000000 # Normalizando
    aux = reads_df[columns].sum(axis=1).sort_values(ascending=False)
    reads_df = reads_df.iloc[aux.index,:]
    reads_df.index = [i for i in range(len(reads_df.index))]

    return reads_df

# Procura uma espécie na base de dados a partir de seu taxid
def searching_db(db, taxid, mode):
    # Selecionar modo de operação
    if mode == "Std":
        table = "Genomes"
    elif mode == "RefSeq":
        table = "RefSeq"

    # Procurar primeiro o taxid da espécie (ORDENAR POR REPRESENTATIVE GENOME!)
    output = None # Se o valor de output não se alterar nenhuma espécie foi encontrada
    db.execute("SELECT ftp_path FROM "+table+" WHERE taxid = ? ORDER BY refseq_category = 'na', refseq_category = 'representative genome', refseq_category = 'reference genome' LIMIT 1;", (taxid,))

    result = db.fetchall()
    if len(result) == 1:
        result = result[0][0] # Recuperar dados da SQL query
        output = result.split("/")[-1]+"_genomic"
        result = "wget -c "+result+"/"+result.split("/")[-1] # Processar a string
        subprocess.call(result+"_genomic.fna.gz"+" -q", shell=True)
        subprocess.call(result+"_genomic.gff.gz"+" -q", shell=True)
    else:
        # Investigar por Cepa, caso necessário
        db.execute("SELECT ftp_path FROM "+table+" WHERE species_taxid = ? ORDER BY refseq_category = 'na', refseq_category = 'representative genome', refseq_category = 'reference genome' LIMIT 1;", (taxid,))

        result = db.fetchall()
        if len(result) == 1:
            result = result[0][0] # Recuperar dados da SQL query
            output = result.split("/")[-1]+"_genomic"
            result = "wget -c "+result+"/"+result.split("/")[-1] # Processar a string
            subprocess.call(result+"_genomic.fna.gz"+" -q", shell=True)
            subprocess.call(result+"_genomic.gff.gz"+" -q", shell=True)

    return output

def searchSpecies(db, species, refSeqExists):
    output = None

    while(True):
        ans = input("------------ Would you like to enter a taxid of a species similar to '"+species+"' and try your luck finding its genome? (Yes/No)\n")
        ans = ans.upper()

        if ans == "YES" or ans == "Y":
            taxid = int(input("------------ Enter the taxid you would like to use:\n"))

            # Procurar o novo taxid na base de dados padrão
            output = searching_db(db=db, taxid=taxid, mode="Std")
            # Caso não ache o novo taxid na base de dados padrão, tente na base alternativa do RefSeq (Caso exista)
            if output is None and refSeqExists:
                output = searching_db(db=db, taxid=taxid, mode="RefSeq")
            break
        elif ans == "NO" or ans == "N":
            break
        else:
            ghost.coloredPrint("Try again:", color="Red")

    return output

def create_translateTable(genome_file, species, taxid, byGenus=False):
    translate_table = {}

    with open(genome_file+".gtf", "r") as file:
        tsv_reader = csv.reader(file, delimiter="\t")

        for row in tsv_reader:
            # Coluna do gtf com os dados que interessam
            analysis_line = row[8].split(";")

            transcript_id, gene_name = None, None

            for data in analysis_line:
                if "transcript_id" in data:
                  transcript_id = data[15:-1]
                elif "gene_name" in data:
                    gene_name = data[12:-1]

            if byGenus:
                genus = species.split(" ")[0]
                analysis_line = [transcript_id, gene_name, genus, species, taxid]
            else:
                analysis_line = [transcript_id, gene_name, species, taxid]

            if transcript_id not in translate_table:
                translate_table[transcript_id] = analysis_line

    translate_table = list(translate_table.values())

    with open(".translate.txt", "a") as out_file:
        tsv_writer = csv.writer(out_file, delimiter="\t")
        tsv_writer.writerows(translate_table)
